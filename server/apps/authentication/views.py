from django.contrib.auth import authenticate
from django.views.generic.base import View

from apps.authentication.services.jwt import JWTToken
from apps.authentication.services.mixins import TokenRequiredMixin
from apps.authentication.services.responses import ErrorResponse, SuccessResponse


class LoginView(View):
    """Представление для аутентификации по логину и паролю."""

    def post(self, request, *args, **kwargs):
        """Проверяет логин и пароль, генерирует и возвращает токен."""
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if not user:
            return ErrorResponse('incorrect username or password')
        access_token = JWTToken.generate(user)
        return SuccessResponse({'access_token': access_token})


class TestLoginView(TokenRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        """
        Служит для проверки сгенерированного токена.
        Возвращает логин идентифицированного пользователя из токена.
        """
        return SuccessResponse({'user': str(kwargs['token_user'])})
