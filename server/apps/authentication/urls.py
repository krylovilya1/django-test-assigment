from django.urls import path

from apps.authentication.views import LoginView, TestLoginView

urlpatterns = [
    path('login/', LoginView.as_view(), name='auth-login'),
    path('test_login/', TestLoginView.as_view())
]
