from django.conf import settings
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User

from apps.authentication.services.jwt import JWTToken


class TokenAuthBackend(ModelBackend):
    """Кастомный бэкенд аутентификации по токену."""

    def authenticate(self, request, **kwargs):
        """Проверка токена и возврат пользователя."""
        jwt_header = request.headers.get(settings.JWT_HEADER_KEY, None)
        if jwt_header:
            result = JWTToken.decode(jwt_header)
            if result:
                return User.objects.get(pk=result['id'])
