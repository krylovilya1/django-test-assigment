from datetime import datetime

from django.conf import settings
from django.contrib.auth.models import User

import jwt


class JWTToken:
    """Класс, предоставляющий обработку jwt токена."""

    @staticmethod
    def generate(user: User) -> str:
        """Генерация токена."""
        return jwt.encode({
            'id': user.id,
            "exp": datetime.now() + settings.JWT_EXPIRED_TIME
        }, settings.SECRET_KEY, algorithm=settings.JWT_ALGORITHM)

    @staticmethod
    def decode(token: str) -> str | None:
        """Декодирование, проверка, и извлечение полезной нагрузки из токена."""
        try:
            split_value = token.split()
            if len(split_value) == 2 and split_value[0] == 'Bearer':
                return jwt.decode(split_value[1], settings.SECRET_KEY, algorithms=(settings.JWT_ALGORITHM,))
        except (jwt.ExpiredSignatureError, jwt.InvalidSignatureError):
            return None
