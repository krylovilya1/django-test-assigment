from django.http import JsonResponse


class SuccessResponse(JsonResponse):
    """Используется для генерации json ответа со статусом: ok."""

    def __init__(self, data='', *args, **kwargs):
        result_data = {
            'status': 'ok',
            'result': data
        }
        super().__init__(data=result_data, *args, **kwargs)


class ErrorResponse(JsonResponse):
    """Используется для генерации json ответа со статусом: error."""

    def __init__(self, data='', *args, **kwargs):
        result_data = {
            'status': 'error',
            'msg': data
        }
        super().__init__(data=result_data, *args, **kwargs)
