from django.contrib.auth import authenticate
from django.contrib.auth.mixins import AccessMixin

from jwt.exceptions import DecodeError, ExpiredSignatureError, InvalidTokenError

from apps.authentication.services.responses import ErrorResponse


class TokenRequiredMixin(AccessMixin):
    """Миксин для Представлений, требующих наличие токена."""

    permission_denied_message = 'permission denied'
    raise_exception = True

    def handle_no_permission(self):
        return ErrorResponse(self.get_permission_denied_message())

    def dispatch(self, request, *args, **kwargs):
        """Идентифицирует пользователя по токену, добавляет token_user к именованным аргументам Представления."""
        try:
            user = authenticate(request=request)
        except (DecodeError, ExpiredSignatureError, InvalidTokenError):
            return ErrorResponse('invalid token')
        if not user:
            return self.handle_no_permission()
        return super().dispatch(request, token_user=user, *args, **kwargs)
