from django.contrib.auth import get_user_model
from django.test import Client

import pytest

User = get_user_model()


@pytest.fixture
def set_up():
    user = User.objects.create_user(
        username='test_user',
        password='test_password',
        email='test@user.com')
    client = Client()
    response = client.post('/api/auth/login/', {
        'username': 'test_user',
        'password': 'test_password',
    })
    token = response.json()['result']['access_token']
    header = {'HTTP_AUTHORIZATION': f'Bearer {token}'}
    return {
        'user': user,
        'header': header,
        'client': client,
    }


@pytest.mark.django_db
def test_login_by_token(set_up):
    client = set_up['client']
    header = set_up['header']
    response = client.get('/api/auth/test_login/', **header, follow=True)
    assert response.json()['status'] == 'ok'
    assert response.json()['result']['user'] == 'test_user'
