from django.contrib.auth import get_user_model
from django.test import Client

import pytest

from apps.car_parts.models import CarBrand, CarModel, PartType

User = get_user_model()


@pytest.fixture
def set_up():
    user = User.objects.create_user(
        username='test_user',
        password='test_password',
        email='test@user.com')
    CarBrand.objects.bulk_create([
        CarBrand(title='Car Brand 1'),
        CarBrand(title='Car Brand 2')
    ])
    CarModel.objects.bulk_create([
        CarModel(title='Car Model 1', brand_id=CarBrand.objects.get(title='Car Brand 1').id),
        CarModel(title='Car Model 2', brand_id=CarBrand.objects.get(title='Car Brand 2').id)
    ])
    PartType.objects.bulk_create([
        PartType(title='Part Type 1'),
        PartType(title='Part Type 2')
    ])
    client = Client()
    response = client.post('/api/auth/login/', {
        'username': 'test_user',
        'password': 'test_password',
    })
    token = response.json()['result']['access_token']
    header = {'HTTP_AUTHORIZATION': f'Bearer {token}'}
    return {
        'user': user,
        'header': header,
        'client': client,
    }


@pytest.mark.django_db
def test_car_brand_filter(set_up):
    response = set_up['client'].get('/api/car_brand/', data={
        'title': 'Car Brand 1'
    })
    response_data = response.json()
    assert response_data['status'] == 'ok'
    assert len(response_data['result']) == 1
    assert response_data['result'][0]['title'] == 'Car Brand 1'


@pytest.mark.django_db
def test_car_model_filter(set_up):
    response = set_up['client'].get('/api/car_model/', data={
        'title': 'Car Model 1'
    })
    response_data = response.json()
    assert response_data['status'] == 'ok'
    assert len(response_data['result']) == 1
    assert response_data['result'][0]['title'] == 'Car Model 1'
    assert response_data['result'][0]['brand']['title'] == 'Car Brand 1'


@pytest.mark.django_db
def test_part_type_filter(set_up):
    response = set_up['client'].get('/api/part_type/', data={
        'title': 'Part Type 1'
    })
    response_data = response.json()
    assert response_data['status'] == 'ok'
    assert len(response_data['result']) == 1
    assert response_data['result'][0]['title'] == 'Part Type 1'


@pytest.mark.django_db
def test_part_create_filter(set_up):
    response = set_up['client'].post('/api/part/create/', data={
        'car_model': CarModel.objects.get(title='Car Model 1').id,
        'type': PartType.objects.get(title='Part Type 1').id
    }, **set_up['header'])
    response_data = response.json()
    assert response_data['status'] == 'ok'
    assert response_data['result']['car_model']['title'] == 'Car Model 1'
    assert response_data['result']['type']['title'] == 'Part Type 1'


@pytest.mark.django_db
def test_part_filter(set_up):
    response = set_up['client'].post('/api/part/create/', data={
        'car_model': CarModel.objects.get(title='Car Model 1').id,
        'type': PartType.objects.get(title='Part Type 1').id
    }, **set_up['header'])
    response = set_up['client'].get('/api/part/', data={
        'car_model__brand__title': 'Car Brand 1'
    })
    response_data = response.json()
    assert response_data['status'] == 'ok'
    assert len(response_data['result']) == 1
    assert response_data['result'][0]['car_model']['brand']['title'] == 'Car Brand 1'


@pytest.mark.django_db
def test_part_update_filter(set_up):
    response = set_up['client'].post('/api/part/create/', data={
        'car_model': CarModel.objects.get(title='Car Model 1').id,
        'type': PartType.objects.get(title='Part Type 1').id
    }, **set_up['header'])
    part_id = response.json()['result']['id']
    response = set_up['client'].post(f'/api/part/update/{part_id}', data={
        'car_model': CarModel.objects.get(title='Car Model 2').id,
        'type': PartType.objects.get(title='Part Type 2').id
    }, **set_up['header'])
    response_data = response.json()
    assert response_data['status'] == 'ok'
    assert response_data['result']['car_model']['title'] == 'Car Model 2'
    assert response_data['result']['type']['title'] == 'Part Type 2'


@pytest.mark.django_db
def test_part_remove_filter(set_up):
    response = set_up['client'].post('/api/part/create/', data={
        'car_model': CarModel.objects.get(title='Car Model 1').id,
        'type': PartType.objects.get(title='Part Type 1').id
    }, **set_up['header'])
    part_id = response.json()['result']['id']
    response = set_up['client'].get(f'/api/part/remove/{part_id}', **set_up['header'])
    response_data = response.json()
    assert response_data['status'] == 'ok'
    assert response_data['result'] == 'Deleted'
