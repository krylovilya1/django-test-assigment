import os

from django.conf import settings
from django.core.exceptions import FieldError
from django.views.generic.base import View

from apps.authentication.services.mixins import TokenRequiredMixin
from apps.authentication.services.responses import ErrorResponse, SuccessResponse
from apps.car_parts.forms import PartForm
from apps.car_parts.models import CarBrand, CarModel, Part, PartType
from apps.car_parts.services.selectors import custom_filter
from apps.car_parts.services.storages import load_file_to_s3, save_url_to_db
from apps.car_parts.services.utils import model_to_dict_with_relation, qs_to_dict


class BaseFilterView(View):
    """Базовое Представление, определяет get метод для фильтрации по полям моделей."""
    model = None

    def get(self, request, *args, **kwargs):
        try:
            result_queryset = custom_filter(request.GET.dict(), self.model)
        except FieldError:
            return ErrorResponse('Invalid parameters')
        data = qs_to_dict(result_queryset)
        return SuccessResponse(data)


class CarBrandFilterView(BaseFilterView):
    """Возвращает автомобильные марки, возможна фильтрация по полю title."""

    model = CarBrand


class CarModelFilterView(BaseFilterView):
    """Возвращает модели автомобилей, возможна фильтрация по полям title и car_brand__title."""

    model = CarModel


class PartTypeFilterView(BaseFilterView):
    """Возвращает наименования автозапчастей, возможна фильтрация по полю title."""

    model = PartType


class CreatePartView(TokenRequiredMixin, View):
    """
    Создаёт объект Запчасть.

    В теле post запроса необходимы параметры car_model и type, поле published необязательно.
    Необходим токен в заголовках запроса.
    """

    def post(self, request, *args, **kwargs):
        form = PartForm(request.POST)
        if not form.is_valid():
            return ErrorResponse(form.errors)
        form.save()
        instance = form.instance
        response_data = model_to_dict_with_relation(instance)
        return SuccessResponse(response_data)


class UpdatePartView(TokenRequiredMixin, View):
    """
    Обновляет (заменяет) объект Запчасть.

    В теле post запроса необходимы параметры car_model и type, поле published необязательно.
    Адрес post запроса должен оканчиваться на '<int:pk>/', где pk - идентификатор объекта Запчасть.
    Необходим токен в заголовках запроса.
    """

    def post(self, request, pk=None, *args, **kwargs):
        if not pk:
            return ErrorResponse('Part id is empty')
        try:
            instance = Part.objects.get(pk=pk)
        except Part.DoesNotExist:
            return ErrorResponse('Part not found')
        form = PartForm(request.POST, instance=instance)
        if not form.is_valid():
            return ErrorResponse(form.errors)
        instance = form.save()
        response_data = model_to_dict_with_relation(instance)
        return SuccessResponse(response_data)


class RemovePartView(TokenRequiredMixin, View):
    """
    Удаляет объект Запчасть.

    Адрес get запроса должен оканчиваться на '<int:pk>/', где pk - идентификатор объекта Запчасть.
    Необходим токен в заголовках запроса.
    """

    def get(self, request, pk=None, *args, **kwargs):
        if not pk:
            return ErrorResponse('Part id is empty')
        try:
            Part.objects.get(pk=pk).delete()
        except Part.DoesNotExist:
            return ErrorResponse('Part not found')
        return SuccessResponse('Deleted')


class ListPartView(View):
    """
    Возвращает массив объектов Запчасть.

    В get запросе могут быть параметры для фильтрации, в т.ч. with_photo, принимающий значение 0 или 1.
    Ключ может принимать значения: 'car_model__title', 'car_model__brand__title'.
    """

    def get(self, request, *args, **kwargs):
        request_data = request.GET.dict()
        with_photo = request_data.pop('with_photo', None)
        try:
            result_queryset = custom_filter(request_data, Part)
        except FieldError:
            return ErrorResponse('Invalid parameters')
        if with_photo is not None:
            if int(with_photo):
                result_queryset = result_queryset.exclude(photo=[])
            else:
                result_queryset = result_queryset.filter(photo=[])
        response_data = qs_to_dict(result_queryset)
        return SuccessResponse(response_data)


class PhotoView(View):
    """
    Возвращает массив url адресов (в s3) с фотографиями для объекта Запчасть.

    Адрес get запроса должен оканчиваться на '<int:pk>/', где pk - идентификатор объекта Запчасть.
    """

    def get(self, request, pk=None, *args, **kwargs):
        if not pk:
            return ErrorResponse('Part id is empty')
        try:
            part = Part.objects.get(pk=pk)
        except Part.DoesNotExist:
            return ErrorResponse('Part not found')
        photo_urls = part.photo
        return SuccessResponse(photo_urls)


class UploadPhotoView(TokenRequiredMixin, View):
    """
    Получает файл с фотографией для объекта Запчасть, и загружает в s3 хранилище.

    Адрес get запроса должен оканчиваться на '<int:pk>/', где pk - идентификатор объекта Запчасть.
    В теле post запроса необходим параметр file.
    Максимальный объём загружаемого файла в settings.MAX_FILE_SIZE (в байтах).
    Необходим токен в заголовках запроса.
    """

    def post(self, request, pk=None, *args, **kwargs):
        file_obj = request.FILES.get('file', '')
        if file_obj.size > settings.MAX_FILE_SIZE:
            return ErrorResponse('File size exceeds the limit')
        if not pk:
            return ErrorResponse('Part id is empty')
        try:
            part = Part.objects.get(pk=pk)
        except Part.DoesNotExist:
            return ErrorResponse('Part not found')
        file_directory_within_bucket = 'upload_photos/'
        file_path_within_bucket = os.path.join(
            file_directory_within_bucket,
            str(pk),
            file_obj.name,
        )
        file_url = load_file_to_s3(file_path_within_bucket, file_obj)
        if file_url:
            save_url_to_db(file_url, part)
            return SuccessResponse({'file_url': file_url})
        return ErrorResponse(f'File {file_obj.name} already exists')
