from django.conf import settings
from django.db.models.signals import post_delete
from django.dispatch import receiver

from storages.backends.s3boto3 import S3Boto3Storage

from apps.car_parts.models import Part


@receiver(post_delete, sender=Part)
def delete_objects_in_s3(sender, instance, **kwargs):
    """Отчищает хранилище s3, после удаления объекта Запчасть."""
    urls = instance.photo
    media_storage = S3Boto3Storage()
    for url in urls:
        url = url.split(settings.AWS_STORAGE_BUCKET_NAME)[-1]
        media_storage.delete(url)
