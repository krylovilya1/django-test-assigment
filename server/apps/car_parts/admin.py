from django.contrib import admin

from apps.car_parts.models import CarBrand, CarModel, Part, PartType


class CarModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'brand')
    readonly_fields = ('id',)


class CarBrandAdmin(admin.ModelAdmin):
    list_display = ('id', 'title')
    readonly_fields = ('id',)


class PartTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'title')
    readonly_fields = ('id',)


class PartAdmin(admin.ModelAdmin):
    list_display = ('id', 'car_model', 'type', 'published')
    readonly_fields = ('id',)


admin.site.register(CarModel, CarModelAdmin)
admin.site.register(PartType, PartTypeAdmin)
admin.site.register(CarBrand, CarBrandAdmin)
admin.site.register(Part, PartAdmin)
