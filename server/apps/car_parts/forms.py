from django.forms import ModelForm

from apps.car_parts.models import Part


class PartForm(ModelForm):
    """Форма для модели Запчасть."""

    class Meta:
        model = Part
        fields = ['id', 'car_model', 'type', 'photo', 'published']
