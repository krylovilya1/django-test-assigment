from django.db import models


class BaseModel(models.Model):
    """
    Базовый класс для моделей.

    _related_fields - кортеж со всеми внешними ключами для вызова select_related().
    Необходимо для перевода объекта модели в словарь, с учётом всех отношений.
    """

    _related_fields = tuple()

    @classmethod
    def related_fields(cls):
        return cls._related_fields

    class Meta:
        abstract = True


class CarBrand(BaseModel):
    """Автомобильная марка."""

    title = models.CharField(verbose_name="наименование автомобильной марки",
                             max_length=64, unique=True)

    class Meta:
        verbose_name = "автомобильная марка"
        verbose_name_plural = "автомобильные марки"

    def __str__(self):
        return self.title


class CarModel(BaseModel):
    """Модель автомобиля."""

    title = models.CharField(verbose_name="наименование модели автомобиля",
                             max_length=128)
    brand = models.ForeignKey(to=CarBrand, verbose_name="автомобильная марка",
                              on_delete=models.CASCADE, related_name='car_models')

    class Meta:
        verbose_name = "модель автомобиля"
        verbose_name_plural = "модели автомобилей"
        constraints = [
            models.UniqueConstraint(fields=('title', 'brand'), name='unique_car_model'),
        ]

    def __str__(self):
        return f"{self.brand} {self.title}"

    _related_fields = ('brand',)


class PartType(BaseModel):
    """Наименование запчасти."""

    title = models.CharField(verbose_name="наименование запчасти",
                             max_length=256)

    class Meta:
        verbose_name = "наименование запчасти"
        verbose_name_plural = "наименования запчастей"

    def __str__(self):
        return self.title


class Part(BaseModel):
    """Запчасть."""

    car_model = models.ForeignKey(to=CarModel, verbose_name="модель автомобиля",
                                  on_delete=models.CASCADE, related_name='parts')
    type = models.ForeignKey(to=PartType, verbose_name="наименование запчасти",
                             on_delete=models.CASCADE, related_name='parts')
    photo = models.JSONField(verbose_name="фотография запчасти", blank=True,
                             default=list)
    published = models.BooleanField(verbose_name="опубликовано", default=False)

    class Meta:
        verbose_name = "запчасть"
        verbose_name_plural = "запчасти"

    def __str__(self):
        return f"{self.type} {self.car_model}"

    _related_fields = ('car_model', 'car_model__brand', 'type')
