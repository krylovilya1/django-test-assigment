from django.apps import AppConfig


class CarPartsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.car_parts'

    def ready(self):
        import apps.car_parts.signals
