from typing import Type

from django.db.models import QuerySet
from django.forms.models import model_to_dict

from apps.car_parts.models import BaseModel


def qs_to_dict(qs: QuerySet) -> list[dict]:
    """Переводит QuerySet в список словарей."""
    qs = qs.select_related(*qs.model.related_fields()).iterator()
    return [model_to_dict_with_relation(instance) for instance in qs]


def model_to_dict_with_relation(instance: Type[BaseModel], depth=0) -> dict | None:
    """Переводит объект модели в словарь. Также рекурсивно обрабатывает связанные объекты."""
    if depth > 2:
        return
    fields = instance._meta.fields
    modeldict = model_to_dict(instance)
    for field_object in fields:
        if field_object.is_relation:
            try:
                related_object = getattr(instance, field_object.name)
                modeldict[field_object.name] = model_to_dict_with_relation(related_object, depth + 1)
            except AttributeError:
                pass
    return modeldict
