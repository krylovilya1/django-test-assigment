from typing import Type

from django.db.models import QuerySet

from apps.car_parts.models import BaseModel


def custom_filter(data: dict, model: Type[BaseModel]) -> QuerySet:
    """
    Выполняет фильтрацию объектов в модели.
    Словарь data содержит ключ (поле), и значение, по которому происходит фильтрация.
    Примеры ключей для объекта Part: 'car_model__title', 'car_model__brand__title'.
    """
    if not data:
        return model.objects.select_related(*model.related_fields()).all()
    filter_params = {}
    for field, value in data.items():
        field__icontains = f"{field}__icontains"
        filter_params.update({field__icontains: value})
    return model.objects.select_related(*model.related_fields()).filter(**filter_params)
