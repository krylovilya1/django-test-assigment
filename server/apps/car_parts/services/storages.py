from storages.backends.s3boto3 import S3Boto3Storage

from apps.car_parts.models import Part


def save_url_to_db(url: str, part: Part):
    """Сохраняет ссылку на файл в поле photo модели Part."""
    part.photo.append(url)
    part.save()


def load_file_to_s3(path: str, file) -> str:
    """Загружает файл в s3, возвращает ссылку на файл"""
    media_storage = S3Boto3Storage()
    if not media_storage.exists(path):
        media_storage.save(path, file)
        file_url = media_storage.url(path)
        return file_url
