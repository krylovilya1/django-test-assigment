from django.urls import path

from apps.car_parts.views import (CarBrandFilterView, CarModelFilterView, CreatePartView, ListPartView,
                                  PartTypeFilterView, PhotoView, RemovePartView, UpdatePartView, UploadPhotoView)

urlpatterns = [
    path('car_brand/', CarBrandFilterView.as_view()),
    path('car_model/', CarModelFilterView.as_view()),
    path('part_type/', PartTypeFilterView.as_view()),
    path('part/create/', CreatePartView.as_view()),
    path('part/update/<int:pk>', UpdatePartView.as_view()),
    path('part/remove/<int:pk>', RemovePartView.as_view()),
    path('part/', ListPartView.as_view()),
    path('part/get_photo/<int:pk>', PhotoView.as_view()),
    path('part/upload_photo/<int:pk>', UploadPhotoView.as_view()),
]
