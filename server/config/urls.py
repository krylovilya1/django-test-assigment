from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('api/auth/', include('apps.authentication.urls')),
    path('api/', include('apps.car_parts.urls')),
    path('admin/', admin.site.urls),
]
