#!/bin/bash

python manage.py collectstatic --noinput
python manage.py migrate
gunicorn config.wsgi -c ./config/gunicorn.py
