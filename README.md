## Тестовое задание

#### Запуск проекта 
```
$ docker-compose up -d
$ poetry install
$ poetry shell
$ python3 manage.py collectstatic
$ python3 manage.py migrate
$ python3 manage.py 
```

```
$ gunicorn config.wsgi -c ./config/gunicorn.py

или

$ python3 manage.py runserver
```

Документация доступна на 8080 порту

Обязательные переменные окружения

- SECRET_KEY
- AWS_STORAGE_BUCKET_NAME
- AWS_S3_CUSTOM_DOMAIN
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- AWS_S3_ENDPOINT_URL
